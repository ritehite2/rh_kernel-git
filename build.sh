#!/bin/bash

# Build script for building kernel, device tree, and kernel
# Possible options include:
#   -t specifies toolchain path
#   -d build only the device tree

set -e

cd $(dirname $0)

DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-02.00.01.07/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
SPECIFIED_TOOLCHAIN_PATH=''
ARCHITECTURE=arm
CROSS_COMPILER=arm-linux-gnueabihf-
NUM_ARGS="$#"

echo ""

if [ $NUM_ARGS -eq 0 ]
then
	echo "No argments passed, building everything with default toolchain path: $DEFAULT_TOOLCHAIN_PATH"
fi

while getopts mdt: ARG
do
	case "$ARG" in
		t)	#Set toolchain path
			if [ -e "$OPTARG"/arm-linux-gnueabihf-gcc ]
            then
                SPECIFIED_TOOLCHAIN_PATH="$OPTARG"  
            else
                echo "Specified toolchain: "$OPTARG""
                echo "is not correct, exiting"
                exit 1
            fi
            ;;
		d)	#Build Device Tree Only
			echo "Building only the device tree"
            DEVICE_TREE_FLAG=1
			;;
		m)	#Run menuconfig
			MENUCONFIG=1
			;;
		*)	#Unrecognized, does not catch anything that does not have a leading
			echo "-t to specify path to alternate toolchain"
			echo "-d to build device tree only"
			echo "-m launch menuconfig with proper environment variables set"
            echo ""
			exit 1;
	esac;
done

if [ -z "$SPECIFIED_TOOLCHAIN_PATH" ]
then
    TOOL_CHAIN_PATH="$DEFAULT_TOOLCHAIN_PATH"
else
    TOOL_CHAIN_PATH="$SPECIFIED_TOOLCHAIN_PATH"
fi

#strong likelihood that we are in the right place if this file exists
if [ ! -e "$TOOL_CHAIN_PATH"/arm-linux-gnueabihf-gcc ]
then
    echo "Toolchain path $TOOL_CHAIN_PATH does not appear to contain the correct tools, exiting"
    echo ""
    exit 1
fi

export PATH="$TOOL_CHAIN_PATH":"$PATH"
export ARCH="$ARCHITECTURE"
export CROSS_COMPILE="$CROSS_COMPILER"

if [ ! -z "$MENUCONFIG" ]
then
	make menuconfig
	exit 0
fi

#need to do this in case a switch between branches occurred
make clean

if [ -z "$DEVICE_TREE_FLAG" ]
then
    
    echo "#### Building zImage ####"
    echo ""

    make -j8 zImage

    echo "#### Building Kernel Modules ####"
    echo ""

    make -j8 modules

    echo "#### Installing Kernel Modules ####"
    echo ""

    if [ -d ./modules_installed ];
	then
    	echo "Existing modules folder is being removed"
        echo ""
   		rm -r modules_installed
	fi
	
	mkdir modules_installed

	make INSTALL_MOD_PATH=./modules_installed modules_install
fi

echo "#### Building Device Tree ####"
echo ""

make -j8 am335x-evmsk.dtb

echo "Kernel build - COMPLETE"
echo ""

exit 0
