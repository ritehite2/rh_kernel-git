syntax: glob

# MKM Copy of the original .gitignore
# NOTE! Don't add files that are generated in specific
# subdirectories here. Add them in the ".gitignore" file
# in that subdirectory instead.
#
# NOTE! Please use 'git ls-files -i --exclude-standard'
# command after changing this file, to see if there are
# any tracked files which get ignored after the change.
#
# Normal rules
#
.*
*.o
*.o.*
*.a
*.s
*.ko
*.so
*.so.dbg
*.mod.c
*.i
*.lst
*.symtypes
*.order
*.elf
*.bin
*.tar
*.gz
*.bz2
*.lzma
*.xz
*.lz4
*.lzo
*.patch
*.gcno
modules.builtin
Module.symvers
*.dwo

#
# Top-level generic files
#
tags
TAGS
linux
vmlinux
vmlinux-gdb.py
vmlinuz
System.map
Module.markers

#
# Debian directory (make deb-pkg)
#
debian/

#
# tar directory (make tar*-pkg)
#
tar-install/

#
# git files that we don't want to ignore even it they are dot-files
#
!.gitignore
!.mailmap

#
# Generated include files
#
include/config
include/generated
arch/*/include/generated

# stgit generated dirs
patches-*

# quilt's files
patches
series

# cscope files
cscope.*
ncscope.*

# gnu global files
GPATH
GRTAGS
GSYMS
GTAGS

*.orig
*~
\#*#

#
# Leavings from module signing
#
extra_certificates
signing_key.priv
signing_key.x509
x509.genkey

# Kconfig presets
all.config

# Kdevelop4
*.kdev4

#copied from arch/arm/boot
arch/arm/boot/Image
arch/arm/boot/zImage
arch/arm/boot/xipImage
arch/arm/boot/bootpImage
arch/arm/boot/uImage

arch/arm/boot/dts/*.dtb

#copied from arch/arm/boot/compressed
arch/arm/boot/compressed/ashldi3.S
arch/arm/boot/compressed/bswapsdi2.S
arch/arm/boot/compressed/font.c
arch/arm/boot/compressed/lib1funcs.S
arch/arm/boot/compressed/hyp-stub.S
arch/arm/boot/compressed/piggy.gzip
arch/arm/boot/compressed/piggy.lzo
arch/arm/boot/compressed/piggy.lzma
arch/arm/boot/compressed/piggy.xzkern
arch/arm/boot/compressed/piggy.lz4
arch/arm/boot/compressed/vmlinux
arch/arm/boot/compressed/vmlinux.lds

# borrowed libfdt files
arch/arm/boot/compressed/fdt.c
arch/arm/boot/compressed/fdt.h
arch/arm/boot/compressed/fdt_ro.c
arch/arm/boot/compressed/fdt_rw.c
arch/arm/boot/compressed/fdt_wip.c
arch/arm/boot/compressed/libfdt.h
arch/arm/boot/compressed/libfdt_internal.h

#copied from arch/arm/kernel
arch/arm/kernel/vmlinux.lds

#copied from arch/arm/vdso/
arch/arm/vdso/vdso.lds
arch/arm/vdso/vdso.so.raw
arch/arm/vdso/vdsomunge

#copied from drivers/tty/vt/
drivers/tty/vt/consolemap_deftbl.c
drivers/tty/vt/defkeymap.c

#copied form kernel/
kernel/config_data.h
kernel/config_data.gz
kernel/time/timeconst.h
kernel/time/hz.bc
kernel/x509_certificate_list

#copied from lib/
lib/gen_crc32table
lib/crc32table.h
lib/oid_registry_data.c

#copied from scripts/
scripts/conmakehash
scripts/kallsyms
scripts/pnmtologo
scripts/unifdef
scripts/ihex2fw
scripts/recordmcount
scripts/docproc
scripts/sortextable
scripts/asn1_compiler

#copied from scripts/basic/
scripts/basic/fixdep
scripts/basic/bin2c

#copied from scripts/dtc/
scripts/dtc/dtc
scripts/dtc/dtc-lexer.lex.c
scripts/dtc/dtc-parser.tab.c
scripts/dtc/dtc-parser.tab.h

#copied from scripts/genksyms/
scripts/genksyms/*.hash.c
scripts/genksyms/*.lex.c
scripts/genksyms/*.tab.c
scripts/genksyms/*.tab.h
scripts/genksyms/genksyms

#copied from scripts/kconfig/
#
# Generated files
#
scripts/kconfig/config*
scripts/kconfig/*.lex.c
scripts/kconfig/*.tab.c
scripts/kconfig/*.tab.h
scripts/kconfig/zconf.hash.c
scripts/kconfig/*.moc
scripts/kconfig/gconf.glade.h
scripts/kconfig/*.pot
scripts/kconfig/*.mo

#
# configuration programs
#
scripts/kconfig/conf
scripts/kconfig/mconf
scripts/kconfig/nconf
scripts/kconfig/qconf
scripts/kconfig/gconf
scripts/kconfig/kxgettext

#copied from scripts/mod/
scripts/mod/elfconfig.h
scripts/mod/mk_elfconfig
scripts/mod/modpost
scripts/mod/devicetable-offsets.h

#copied from usr/
usr/gen_init_cpio
usr/initramfs_data.cpio
vinitramfs_data.cpio.gz
usr/initramfs_data.cpio.bz2
usr/initramfs_data.cpio.lzma
usr/initramfs_list
usr/include

#Created modules folder for deployment
syntax: regexp
^modules_installed/
