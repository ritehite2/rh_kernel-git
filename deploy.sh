#!/bin/bash

# Deploy script for the kernel, device tree, and modules

set -e

cd $(dirname $0)

#assume that if the boot folder exists the root file system is in place
DEFAULT_DEPLOY=../deploy/rootfs
DEVICE_TREE_LOCATION=./arch/arm/boot/dts/am335x-evmsk.dtb
KERNEL_LOCATION=./arch/arm/boot/zImage
MODULES_LOCATION=./modules_installed/lib
NUM_ARGS=$#
STARTING_LOCATION="$PWD"

if [ "$NUM_ARGS" -eq "1" ]
then
	DEPLOY_PATH="$1"
else
	DEPLOY_PATH="$DEFAULT_DEPLOY"
fi

if [ ! -e "$DEPLOY_PATH" ]
then
	echo "Deploy path does not exist, exiting"
	exit 1
fi

if [ ! -e "$DEVICE_TREE_LOCATION" ] || [ ! -e "$KERNEL_LOCATION" ] || [ ! -e "$MODULES_LOCATION" ]
then
	echo "Missing deployment files, build may not have been run, exiting"
	exit 1
fi

echo "#### Deploying Kernel and Device Tree ####"
echo ""

#remove the old files or any files for that matter
find "$DEPLOY_PATH/boot" -mindepth 1 -delete
cp "$DEVICE_TREE_LOCATION" "$DEPLOY_PATH/boot"
cp "$KERNEL_LOCATION" "$DEPLOY_PATH/boot"

echo "#### Deploying Modules ####"
echo ""

#Currently building the graphics driver with the root file system
EXTRA_PATH=$(find "$DEPLOY_PATH/lib/modules" -maxdepth 2 -name extra)
cp -r $EXTRA_PATH ./
find "$DEPLOY_PATH/lib/modules" -mindepth 1 -delete
cp -r "$MODULES_LOCATION"/firmware/* "$DEPLOY_PATH"/lib/firmware/

#Should only be one kernel modules folder
MODULES_NAME=$(find "$MODULES_LOCATION"/modules -maxdepth 1 -name 4.1.13*)
cp -r "$MODULES_LOCATION"/modules/"${MODULES_NAME##*/}" "$DEPLOY_PATH"/lib/modules
mv ./extra "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"

echo "alias of:N*T*Cti,dra7-sgx544* omapdrm_pvr" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.alias
echo "alias of:N*T*Cti,am4376-sgx530* omapdrm_pvr" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.alias
echo "alias of:N*T*Cti,am3352-sgx530* omapdrm_pvr" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.alias

echo "extra/omapdrm_pvr.ko:" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.dep

echo "alias symbol:PVRGetDisplayClassJTable omapdrm_pvr" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.symbols
echo "alias symbol:PVRGetBufferClassJTable omapdrm_pvr" >> "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"/modules.symbols

cp rh_additions/* "$DEPLOY_PATH"/lib/modules/"${MODULES_NAME##*/}"

echo "Kernel deploy - COMPLETE"
echo ""

exit 0
